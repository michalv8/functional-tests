<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name = "users")
 */
class User
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type = "guid")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type = "string", length = 64, unique = true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type = "string", length = 64)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(type = "string")
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type = "string")
     */
    private $lastName;

    /**
     * @var bool
     *
     * @ORM\Column(type = "boolean")
     */
    private $active;

    public function __construct(string $username, string $password, string $firstName, string $lastName, bool $active = true)
    {
        $this->id = (string) Uuid::uuid4();
        $this->username = $username;
        $this->password = $password;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->active = $active;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function isActive(): bool
    {
        return $this->active;
    }
}
