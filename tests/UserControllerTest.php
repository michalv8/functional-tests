<?php

declare(strict_types=1);

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testGetUsers(): void
    {
        $client = static::createClient();

        $client->xmlHttpRequest('GET', '/users');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }
}